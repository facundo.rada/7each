# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Purpose: Script to compress all files or folders to individual archives using 7z
# Usage  : ./7each [-d][-o "<Output Folder>"][-v] i.e ./7each -d -o "compress" -v
# Author : Zeng-He
# Date   : 25/04/2016
#
#!/bin/bash

###Variables###
#Argument flags
verbose=false # show info 'bout the process
includeFolders=false # include directories
output=false # set directory output
#Internal
process=./*.* # Process only files

#Handle optional parameters
while getopts 'do:v' flag; do
  case "${flag}" in
    d) includeFolders=true ;;
    o) output="./${OPTARG}" ;;
    v) verbose=true ;;
    *) error "Unexpected option ${flag}" ;;
  esac
done

#Show info
if [ $verbose == true ]; then
  echo -d "process directories" $includeFolders
  echo -o "set output" $output
fi

if [ $includeFolders == true ]; then
  process=./* # Include folders
else
  process=./*.* # Process only files
fi

if [ "$output" != false ]; then # Have directory output
  mkdir $output #doesn't care if it already exists
fi

for file in $process; do
  if [ "$file" == "$0" ]; then 
    continue # skip yourself
  elif [ $includeFolders == true ] && [ "$output" == "$file" ]; then # Include folders
      continue #skip directory output
  fi

  7z a -mx9 -mmt "$file".7z "$file" #the only line that matters

  #move file to output
  if [ "$output" != false ]; then
    cp "$file".7z "$output"
    rm "$file".7z
  fi
done

exit 0 