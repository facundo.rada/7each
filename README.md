
Bugs conocidos:
Si la opcion -d NO esta presente, el argumento de búsqueda es *.*, o sea trabajar solo archivos, pero esto no va a procesar archivos sin extensión.. gracias linux



Oportunidades de mejora:
Esto puede expandirse hasta el infinito si quieren, perdiendo su escencia y volviendose alguna solución genérica que haga de todo un poco.. pero siendo realistas, eso no va a pasar ni en pedo. Por lo que les dejo algunas funcionalidades que se me ocurrieron mientras lo estaba haciendo. No las agregue xq asi como está ya cumplia con su trabajo, ademas como les dije, yo simplemente queria hacer le trabajo rapido y no es mi idea gastar tiempo en un super script que no iba a poder explotar. En fin, aca hay algunas cosas que se le pueden agregar:

opcion -h para mostrar ayuda y formas de uso

opcion -s para especificar una lista de extensiones a evitar:
-s "7z rar zip tar.gz"

checkear si el programa 7z está instalado:
got7z=$(which 7z) # Use "which" to check if there's a copy of 7z on the system
 
# If there's no copy of 7z - we're not going to be doing much compressing... Exit stage left.
if [ "$got7z" = "" ]; then
    echo "No copy of 7z found on system!"
    exit 0
fi


Que se vaya metiendo en las carpetas y procesar recursivamente. (Thanks nbiocca)
(Deberia de obligatoriamente usar la opcion -d)

if [$includeFolders == true ] && [ $recursive == true ]; then
    7each $includeFolders $recursive $verbose $output
fi